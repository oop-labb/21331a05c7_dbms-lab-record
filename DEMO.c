#include <stdio.h>
#include <stdlib.h>

struct Employee {
    char name[50];
    int id;
    char pos[40];
    int age;
    float salary;
};

void storeEmployeeDetails() {
    struct Employee emp;
    FILE *file;

    file = fopen("file.txt", "a");
    if (file == NULL) {
        printf("Unable to open file.\n");
        return;
    }

    printf("\nEnter employee name: ");
    scanf("%s", emp.name);
    printf("Enter employee id: ");
    scanf("%d", &emp.id);
    printf("Enter employee designation : ");
    scanf("%s", emp.pos);
    printf("Enter employee age: ");
    scanf("%d", &emp.age);
    printf("Enter employee salary: ");
    scanf("%f", &emp.salary);

    fprintf(file, "%s %d %s %d %.2f \n", emp.name, emp.id, emp.pos, emp.age, emp.salary);

    fclose(file);
}

void retrieveEmployeeDetails() {
    struct Employee emp;
    FILE *file;

    file = fopen("file.txt", "r");
    if (file == NULL) {
        printf("Unable to open file.\n");
        return;
    }

    printf("Employee Details:\n");

    while (fscanf(file, " %s %d %s %d %f\n ", emp.name, &emp.id, emp.pos, &emp.age, &emp.salary) != EOF) {
        printf("Name :%s, id :%d, designation :%s , Age :%d, Salary: %f\n", emp.name, emp.id, emp.pos, emp.age, emp.salary);
    }

    fclose(file);
}

int main() {
    int choice;

    while (1) {
        printf("\nEmployee Database\n");
        printf("1. Add Employee\n");
        printf("2. Retrieve Employee Details\n");
        printf("3. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                storeEmployeeDetails();
                break;
            case 2:
                retrieveEmployeeDetails();
                break;
            case 3:
                printf("Exiting program.\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
